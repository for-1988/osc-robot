package me.feng.chatterbot

import akka.actor.Actor
import com.google.code.chatterbotapi.ChatterBotFactory
import com.google.code.chatterbotapi.ChatterBotSession
import com.google.code.chatterbotapi.ChatterBot
import com.google.code.chatterbotapi.ChatterBotType
import me.feng.osc.Active

class ChatterActor extends Actor {

  def receive = {
    case active: Active => {
      val factory = new ChatterBotFactory();
      val bot2 = factory.create(ChatterBotType.CLEVERBOT);
      val bot2session = bot2.createSession();
      val s = bot2session.think(active.getMessage);
      active.setReplymsg(s)
      sender ! active
    }
  }
}