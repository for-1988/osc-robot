package me.feng.osc

import java.net.URLEncoder
import scala.concurrent.duration.FiniteDuration
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler
import org.apache.commons.httpclient.Header
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.cookie.CookiePolicy
import org.apache.commons.httpclient.methods.GetMethod
import org.apache.commons.httpclient.params.HttpMethodParams
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import me.feng.chatterbot.ChatterActor
import scala.actors.threadpool.TimeUnit
import scala.concurrent.duration.Duration
import org.jsoup.Jsoup

class OscActor extends Actor {

  val client = new HttpClient

  // 设置 HttpClient 接收 Cookie,用与浏览器一样的策略
  client.getParams().setCookiePolicy(
    CookiePolicy.BROWSER_COMPATIBILITY);
  // 设置 默认的超时重试处理策略
  client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
    new DefaultHttpMethodRetryHandler());
  // 设置 连接超时时间
  client.getHttpConnectionManager().getParams()
    .setConnectionTimeout(20000);
  // 设置 读数据超时时间
  client.getHttpConnectionManager().getParams()
    .setSoTimeout(20000);
  // 设置 字符集
  client.getParams().setContentCharset("UTF-8");

  val username = "for1988@126.com"
  val password = "1111"

  var u: User = _

  def receive = {
    case "login" => {
      val login_url = OscUrl.LOGIN_VALIDATE_HTTP
      val login = _get(login_url + "?username=" + username + "&pwd=" + password + "&keep_login=1")
      val head = new Header
      client.executeMethod(login)
      println(login.getResponseBodyAsString())
      u = User.parse(login.getResponseBodyAsString())
      login.releaseConnection()

      
    }

    case "check" => {

      val notice_url = OscUrl.USER_NOTICE
      val notice = _get(notice_url + "?uid=" + u.getUid)
      client.executeMethod(notice)
      val xml = notice.getResponseBodyAsString()
      val atme = Jsoup.parse(xml)
      val count = atme.select("atmeCount").first().html().toInt
      self ! count
      notice.releaseConnection()

      val clear_notice_url = OscUrl.NOTICE_CLEAR
      val clear = _get(clear_notice_url + "?uid=" + u.getUid + "&type=" + 1)
      client.executeMethod(clear)
      clear.releaseConnection()
    }

    case count: Int => {
      val active_url = OscUrl.ACTIVE_LIST
      val active = _get(active_url + "?uid=1240916&catalog=2&pageIndex=0&pageSize=" + count)
      client.executeMethod(active)
      val list = Active.parseList(active.getResponseBodyAsString())
      list.foreach(active => {
        val chatter = context.actorOf(Props[ChatterActor])
        chatter ! active
      })
      active.releaseConnection()
    }

    case active: Active => {
      val reply_url = OscUrl.COMMENT_REPLY
      val url = reply_url + "?catalog=" + active.getCatalog + "&id=" + active.getId + "&uid=" + u.getUid + "&content=" + URLEncoder.encode(active.getReplymsg) + "&replyid=" + active.getReplyid + "&authorid=" + active.getAuthorid
      val reply = _get(url)
      client.executeMethod(reply)
      reply.releaseConnection()

    }
  }

  def _get(url: String): GetMethod = {
    val method = new GetMethod(url)
    method.setRequestHeader("Host", "www.oschina.net")
    method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    method.setRequestHeader("Accept-Language", "zh-CN,zh;q=0.8")
    method.setRequestHeader("Connection", "keep-alive")
    method.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36")
    method
  }
}

