package me.feng.osc

class OscApi {

}

object OscUrl{
  val HOST = "www.oschina.net"
  val HTTP = "http://"
  val HTTPS = "https://"
    
  val URL_SPLITTER = "/"
  val URL_UNDERLINE = "_"
    
  val URL_API_HOST = HTTP + HOST + URL_SPLITTER
  
  val LOGIN_VALIDATE_HTTP = URL_API_HOST + "action/api/login_validate"
  val LOGIN_VALIDATE_HTTPS = HTTPS + HOST + URL_SPLITTER + "action/api/login_validate"
  
  val ACTIVE_LIST = URL_API_HOST + "action/api/active_list"
  
  val USER_NOTICE = URL_API_HOST + "action/api/user_notice";
  
  val COMMENT_REPLY = URL_API_HOST + "action/api/comment_reply"
  
  val NOTICE_CLEAR = URL_API_HOST + "action/api/notice_clear";
}