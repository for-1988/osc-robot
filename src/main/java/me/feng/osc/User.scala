package me.feng.osc

import scala.beans.BeanProperty
import org.jsoup.Jsoup

class User {

  @BeanProperty
  var uid: String = _

}

object User {

  def parse(xml: String): User = {
    val u = new User
    val ele = Jsoup.parse(xml)
    u.setUid(ele.select("uid").first().html())
    u
  }
}